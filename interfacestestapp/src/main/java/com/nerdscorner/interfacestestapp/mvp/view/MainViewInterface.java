package com.nerdscorner.interfacestestapp.mvp.view;

public interface MainViewInterface {
    void setTextValue(CharSequence value);
}
