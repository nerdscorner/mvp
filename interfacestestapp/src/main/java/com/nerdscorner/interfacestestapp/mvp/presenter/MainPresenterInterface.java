package com.nerdscorner.interfacestestapp.mvp.presenter;

public interface MainPresenterInterface {
    void onActionClicked();

    void onBackgroundTaskCompleted();
}
